
Very Important:

LICENSE
___________________________________________________________

The Photoshop (.psd) and .tga files provided in this folder are for your PERSONAL USE ONLY.
Personal use means non-commercial use of these textures. You may not redistribute these 
Photoshop files neither modified or unmodified. You may not redistribute these files in 
this (.psd) or any other format, including, but not limited to (.tga, .jpg, .png, .bmp). 
These textures may not be used in any way whatsoever in which you charge money, collect fees, 
or receive any form of remuneration. 

By downloading these files you accept these terms / conditions.

____________________________________________________________

~ aeros